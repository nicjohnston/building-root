#!/bin/bash

#settings
root_version=v5-34-00-patches
xrootd_version=4.8.5
basedir=root_v5.34.00-patches
build_threads=`nproc`


#install tools and root prereqs
sudo apt install git dpkg-dev cmake g++ gcc binutils libx11-dev libxpm-dev libxft-dev libxext-dev gfortran libssl-dev libpcre3-dev xlibmesa-glu-dev libglew1.5-dev libftgl-dev libmysqlclient-dev libfftw3-dev libcfitsio-dev graphviz-dev libavahi-compat-libdnssd-dev libldap2-dev python-dev libxml2-dev libkrb5-dev libgsl0-dev libqt4-dev 

#Download root and switch to 5.34.00 patches branch
homedir=`pwd`
git clone http://github.com/root-project/root.git $basedir
cd $basedir 
git checkout $root_version

#build Xrootd
sudo mkdir /opt/xrootd
sudo build/unix/installXrootd.sh -j$build_threads --version=$xrootd_version /opt/xrootd

cd $homedir
mkdir $basedir-build
cd $basedir-build
cmake -Dall=ON -Dmt=ON -Dfftw3=ON -Dcxx14=ON -Dcxx17=ON  -Dxrootd=ON -DXROOTD_ROOT_DIR=/opt/xrootd/xrootd-$xrootd_version/ -DCMAKE_INSTALL_PREFIX=$homedir/$basedir-install $homedir/$basedir
cmake --build . -- -j$build_threads
cmake --build . --target install
source $homedir/$basedir-install/bin/thisroot.sh
root



