# Building Root
The [build-root-v5.sh](build-root-v5.sh) script works for Ubuntu and Debian testing.
Currently tested on Ubuntu 19.04 and Debian testing, but should also work for Ubuntu 18.

The [build-root-v6.sh](build-root-v6.sh) script works on Ubuntu 19.04, is undergoing testing on Ubuntu 18.04, and works on Debian testing (Buster) except for a [minor bug](https://gitlab.com/nijo5747/building-root/issues/1)

# PyROOT
Both versions appear to work with Python 2.

Python 3 support is more complicated and not working right now.

# Jupyter
ROOTbooks are currently non-functional

# TODO:
Figure out Python 3/ROOT integration

Fix ROOT Jupyter notebook issue
