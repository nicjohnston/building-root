#!/bin/bash

#settings
root_version=v6-16-00
xrootd_version=4.9.1
basedir=root_v6.16.00
build_threads=`nproc`
#build_threads=4
homedir=`pwd`


#install tools and root prereqs
sudo apt install git dpkg-dev cmake g++ gcc binutils libx11-dev libxpm-dev libxft-dev libxext-dev gfortran libssl-dev libpcre3-dev xlibmesa-glu-dev libglew1.5-dev libftgl-dev libmysqlclient-dev libfftw3-dev libcfitsio-dev graphviz-dev libavahi-compat-libdnssd-dev libldap2-dev python-dev libxml2-dev libkrb5-dev libgsl0-dev libqt4-dev 

#Download root and switch to 5.34.00 patches branch
git clone http://github.com/root-project/root.git $basedir
cd $basedir 
git checkout $root_version

#build Xrootd
cd $homedir
wget http://www.xrootd.org/download/v$xrootd_version/xrootd-$xrootd_version.tar.gz
tar -xf xrootd-$xrootd_version.tar.gz
mkdir xrootd-$xrootd_version/build
cd xrootd-$xrootd_version/build
cmake ../ -DCMAKE_INSTALL_PREFIX=$homedir/xrootd-$xrootd_version-install -DENABLE_PERL=FALSE
make -j$build_threads
make install


cd $homedir
mkdir $basedir-build
cd $basedir-build
#cmake -Dall=ON -Dmt=ON -Dfftw3=ON -Dcxx14=ON -Dcxx17=ON  -Dxrootd=ON -DXROOTD_ROOT_DIR=/opt/xrootd/xrootd-$xrootd_version/ -DCMAKE_INSTALL_PREFIX=$homedir/$basedir-install $homedir/$basedir
echo cmake -Dall=ON -Dcxx14=ON -Dcxx17=ON -Dxrootd=ON -DXROOTD_ROOT_DIR=$homedir/xrootd-$xrootd_version-install -DCMAKE_INSTALL_PREFIX=$homedir/$basedir-install $homedir/$basedir
cmake -Dall=ON -Dcxx14=ON -Dcxx17=ON -Dxrootd=ON -DXROOTD_ROOT_DIR=$homedir/xrootd-$xrootd_version-install -DCMAKE_INSTALL_PREFIX=$homedir/$basedir-install $homedir/$basedir
cmake --build . -- -j$build_threads
cmake --build . --target install
source $homedir/$basedir-install/bin/thisroot.sh
root



